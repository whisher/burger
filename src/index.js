// Core
import React from 'react';
import ReactDOM from 'react-dom';

// Service Worker
import registerServiceWorker from './registerServiceWorker';



// Styles
import './assets/styles/index.css';
import './styles.css';

// App
import App from './App';

// Fonts
import FontFaceObserver from 'fontfaceobserver';

// Observe loading of Open Sans (to remove open sans, remove the <link> tag in
// the index.html file and this observer)
const openSansObserver = new FontFaceObserver('Open Sans', {});

// When Open Sans is loaded, add a font-family using Open Sans to the body
openSansObserver.load().then(() => {
  document.body.classList.add('fontLoaded');
}, () => {
  document.body.classList.remove('fontLoaded');
});

const app = (
  <App />
);
const MOUNT_NODE = document.getElementById('app');

ReactDOM.render(app, MOUNT_NODE);
registerServiceWorker();
