import * as actionTypes from './actionTypes';
import axios from '../../axios';

export const loadMenuFail = () => {
    return {
        type: actionTypes.LOAD_MENU_FAIL
    };
};

export const loadMenuSuccess = ( menu ) => {
    return {
        type: actionTypes.LOAD_MENU_SUCCESS,
        payload: menu
    };
};

export const loadMenu = () => {
    return dispatch => {
        axios.get( '/menu' )
            .then( response => {
               dispatch(loadMenuSuccess(response.data));
            } )
            .catch( error => {
                dispatch(loadMenuFail());
            } );
    };
};
