import * as actionTypes from '../actions/actionTypes';

const initialState = {
  ingredients: []
};

const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
    case actionTypes.ADD_INGREDIENT:
      return {
        ...state,
        ingredients: []
      }
      case actionTypes.REMOVE_INGREDIENT:
        return {
          ...state,
          ingredients: []
      }
      default:
        return state;
    }
};

export default reducer;
