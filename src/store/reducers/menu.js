import * as actionTypes from '../actions/actionTypes';

const initialState = {
  error: false,
  loading: true,
  menu: {},
};

const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
    case actionTypes.LOAD_MENU:
      return {
        ...state,
        loading: true
      }
      case actionTypes.LOAD_MENU_FAIL:
        return {
          ...state,
          error: true,
          loading: false
      }
      case actionTypes.LOAD_MENU_SUCCESS:
        return {
          ...state,
          error: false,
          loading: false,
          menu: action.payload
      }
      default:
        return state;
    }
};

export default reducer;
