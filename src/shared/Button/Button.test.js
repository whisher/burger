import React from 'react';

import { configure, shallow } from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import { Minus } from '../Icons/Icons';
import Button from './Button';


configure({adapter: new Adapter()});

describe('<Button />', () => {
    let wrapper;

    beforeEach(() => {
        wrapper = shallow(<Button><Minus /></Button>);
    });

    it('should render Minus', () => {
        expect(wrapper.contains(<Minus />));
    });

});
