import React from 'react';

import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faMinus,
  faPlus,
  faSpinner
} from '@fortawesome/free-solid-svg-icons';

library.add(
  faMinus,
  faPlus,
  faSpinner
);

export const Spinner = ({className = ''}) => (
  <FontAwesomeIcon className={className} icon="spinner" pulse/>
);

export const Minus = ({className = ''}) => (
  <FontAwesomeIcon className={className} icon="minus"/>
);

export const Plus = ({className = ''}) => (
  <FontAwesomeIcon className={className} icon="plus"/>
);
