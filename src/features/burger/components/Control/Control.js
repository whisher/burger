import React from 'react';

import Button from '../../../../shared/Button/Button';
import {
  Minus,
  Plus
} from '../../../../shared/Icons/Icons';

import './styles.css';

const Control = ({ingredient}) =>
  <li>
    <span>{ingredient.name}</span>
    <Button><Minus /></Button>
    <Button><Plus /></Button>
  </li>


export default Control;
