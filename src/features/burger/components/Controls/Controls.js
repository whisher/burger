import React from 'react';
import Control from '../Control/Control';

import './styles.css';

const Controls = ({ingredients}) => (
    <div className="burger-controls">
      <ul>
        {ingredients.map((ingredient, i )=> (
            <Control
                key={i}
                ingredient={ingredient}
                />
        ))}
      </ul>
    </div>
);

export default Controls;
