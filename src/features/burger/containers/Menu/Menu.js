import React, {Component} from 'react';
import { connect } from 'react-redux';

import * as actions from '../../../../store/actions';
import { Spinner } from '../../../../shared/Icons/Icons';
import Controls from '../../components/Controls/Controls';
import './styles.css';

class Menu extends Component {

  componentDidMount () {
      this.props.onLoadMenu();
  }

  render() {
    let output = null;
    if(this.props.error){
      output = <p>Something go wrong</p>;
    }
    else if(this.props.loading){
      output = <Spinner className="fa-3x" />;
    }
    else{
      output = <Controls ingredients={this.props.menu.ingredients} />;
    }
    return (
      <div className="burger-menu">
      {output}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    error: state.menuState.error,
    loading: state.menuState.loading,
    menu: state.menuState.menu
  };
}

const mapDispatchToProps = dispatch => {
  return {
    onLoadMenu: () => dispatch(actions.loadMenu()),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);
