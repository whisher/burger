import React from 'react';

import './styles.css';
import burgerLogo from '../../../assets/images/logo.png';

const Brand = () => (
    <div className="container">
      <img className="burger-brand" src={burgerLogo} alt="burger" />
    </div>
);

export default Brand;
