import React from 'react';

import './styles.css';

const Footer = () => (
  <div className="container">&#169;Burger - 2018</div>
);

export default Footer;
