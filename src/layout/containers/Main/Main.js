import React from 'react';

import { Brand, Footer } from '../../components';

import './styles.css';

const Main = ({children}) => (
  <div className="burger">
    <header className="burger-header">
      <Brand />
    </header>
    <main className="burger-main">
      <div className="container">
        {children}  
      </div>
    </main>
    <footer className="burger-footer">
      <Footer />
    </footer>
  </div>
);

export default Main;
