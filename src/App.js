import React, { Component } from 'react';

// Router
import {
  BrowserRouter as Router,
  Route,
  Switch,
 Redirect } from 'react-router-dom';

 // Redux
 import { Provider } from 'react-redux';
 import store from './store/configureStore';

import { Main } from './layout/containers';
import { Burger, Checkout, Order } from './features';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <Router>
          <Main>
            <Switch>
              <Route path="/checkout" component={Checkout} />
              <Route path="/order" component={Order} />
              <Route path="/" exact component={Burger} />
              <Redirect from='*' to='/' />
            </Switch>
          </Main>
        </Router>
      </Provider>
    );
  }
}

export default App;
